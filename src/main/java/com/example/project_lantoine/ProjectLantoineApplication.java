package com.example.project_lantoine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@SpringBootApplication
@Configuration
@ComponentScan(basePackages = "com.example.project_lantoine")
public class ProjectLantoineApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectLantoineApplication.class, args);
	}

}
