package com.example.project_lantoine.entities;
import javax.persistence.*;
import java.util.List;

@Entity
public class Personne {
	//Id généré automatiquement)
    @Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Long id;
	//Prénom de la personne
	private String prenom;
	//Age de la personne
	private int age;
	//Liste de Todo associée à la personne en ManyToMany
	@ManyToMany(cascade = CascadeType.MERGE, mappedBy = "personne", fetch = FetchType.EAGER)
	private List<Todo> todos;
	
	//Constructeurs 

	public Personne() {
		super();
	}
	
	public Personne(String prenom, int age) {
		super();
		this.prenom = prenom;
		this.age = age;
	}

	//Getters/Setters
	
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Todo> getTodos() {
		return todos;
	}

	public void setTodo(List<Todo> todos) {
		this.todos = todos;
	}
    
}
