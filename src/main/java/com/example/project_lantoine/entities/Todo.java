package com.example.project_lantoine.entities;

import java.io.Serializable;

import javax.persistence.*;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Entity;

@Entity
@Table(name = "todo")
public class Todo implements Serializable{
    //Id généré automatiquement
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    //Nom de la tâche
    private String name;
    //Description de la tâche
    private String description;
    //Priorité de la tâche
    private String urgent;
    //Association à une personne en ManyToOne car une personne peut avoir plusieurs todos
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnoreProperties({"todos"})
    @JoinColumn(name = "personne_id")
    private Personne personne;

    //Constructeurs

    public Todo() {
        super();
    }

    public Todo(String name, String description, Personne p, String urgent){
        super();
        this.name = name;
        this.personne = p;
        this.description = description;
        this.urgent = urgent;
    }

    //Getters/Setters

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Personne getPersonne(){
        return personne;
    }

    public void setPersonne(Personne p){
        this.personne = p;
    }

    public String getUrgent(){
        return urgent;
    }

    public void setUrgent(String u){
        this.urgent = u;
    }
}
