package com.example.project_lantoine.repositories;

import com.example.project_lantoine.entities.Todo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TodoRepository extends JpaRepository<Todo, Long> { }
