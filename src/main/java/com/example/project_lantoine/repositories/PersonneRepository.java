package com.example.project_lantoine.repositories;

import com.example.project_lantoine.entities.Personne;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonneRepository extends JpaRepository<Personne, Long> { }