package com.example.project_lantoine.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.websocket.server.PathParam;
import javax.ws.rs.core.Response;

import com.example.project_lantoine.entities.Personne;
import com.example.project_lantoine.entities.Todo;
import com.example.project_lantoine.repositories.PersonneRepository;
import com.example.project_lantoine.repositories.TodoRepository;

@RestController
@RequestMapping("/personnes")
public class PersonnesController {

    @Autowired
    private final PersonneRepository personneRepository;
    @Autowired
	private TodoRepository todoRepository;

    public PersonnesController(PersonneRepository personneRepository) {
        this.personneRepository = personneRepository;
    }


	//Créer une personne, méthode POST qui ajoute la personne dans la BDD.
	 @PostMapping
    public ResponseEntity<Personne> createPersonne(@RequestBody Personne personne) throws URISyntaxException {
        Personne savedClient = personneRepository.save(personne);
        return ResponseEntity.created(new URI("/personnes/" + savedClient.getId())).body(savedClient);
    }

	//Afficher toutes les personnes, méthode GET qui récupère les personnes de la BDD
    @GetMapping()
    public List<Personne> getAllPersonne() {
		List<Personne> personnes = new ArrayList<>();
		personneRepository.findAll().forEach(personnes::add);
		return personnes;
	}

    @PatchMapping("/{id}")
    public Response updateAge(@PathParam("id") Long id, Personne p) {
		int age = p.getAge();
		Optional<Personne> optional = personneRepository.findById(id);

		if (optional.isPresent()) {
			Personne pBDD = optional.get();
			pBDD.setAge(age);
			personneRepository.save(pBDD);
			return Response.ok(pBDD).build();
		} else {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}

	//Afficher une personne (via son id), méthode GET
    @GetMapping("/{id}")
    public Personne getPersonne(@PathVariable Long id) {
        return personneRepository.findById(id).orElseThrow(RuntimeException::new);
    }

    @PutMapping("/{id}")
    public Personne updateTotalyPersonne(@PathParam("id") Long id, Personne p) {
		p.setId(id);
		return personneRepository.save(p);
	}

	//Supprimer une personne, méthode DELETE qui supprime une persone dans la BDD
    @DeleteMapping("/{id}")
	public Response deletePersonne(@PathVariable("id") Long id) {
		if (personneRepository.findById(id).isPresent()) {
			Personne p = personneRepository.findById(id).get();
			p.getTodos().forEach(todo -> todoRepository.deleteById(todo.getId()));;
			personneRepository.deleteById(id);
		}
		return Response.noContent().build();
	}

	//Afficher les todos d'une personne (via l'id de la personne), méthode GET
    @GetMapping("/{id}/todos")
    public List<Todo> listerTodo(@PathVariable("id") Long id) {
		return personneRepository.findById(id).get().getTodos();
	}

	//Afficher uniquement le prénom d'une personne (via l'id de la personne), méthode GET
    @GetMapping("/{id}/prenom")
    public String listerPrenom(@PathVariable("id") Long id){
        return personneRepository.findById(id).get().getPrenom();    }

	//Ajouter une todo à une personne (via l'id de la personne), méthode POST qui écrit dans la BDD
    @PostMapping("/{personne_id}/todos")
    public Response addTodoDejaExistant(@PathVariable("personne_id") Long id, TodoInput todos) {
        System.out.println(todos.getIdTodo());
       
		Optional<Personne> pOpt = personneRepository.findById(id);
		Optional<Todo> tOpt = todoRepository.findById(todos.getIdTodo());
        System.out.println(tOpt);

		if (!pOpt.isPresent() || !tOpt.isPresent()) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
		Personne p = pOpt.get();
		Todo t = tOpt.get();
		p.getTodos().add(t);
		personneRepository.save(p);
		return Response.ok(p).build();
	}
}
