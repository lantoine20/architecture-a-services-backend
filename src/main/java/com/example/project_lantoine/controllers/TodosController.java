package com.example.project_lantoine.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import com.example.project_lantoine.entities.Todo;
import com.example.project_lantoine.repositories.TodoRepository;

@RestController
@RequestMapping("/api")
public class TodosController {

    @Autowired
    private final TodoRepository todoRepository;

    public TodosController(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    //Afficher toutes les todos, méthode GET qui affiche les todos de la BDD
    @GetMapping("/todos")
    public List<Todo> getTodos() {
        return todoRepository.findAll();
    }

    //Afficher une seule todo en fonction de son id, méthode GET
    @GetMapping("/{id}")
    public Todo getTodo(@PathVariable Long id) {
        return todoRepository.findById(id).orElseThrow(RuntimeException::new);
    }

    //Créer une todo, méthode POST qui écrit dans la BDD
    @PostMapping
    public ResponseEntity<Todo> createTodo(@RequestBody Todo todo) throws URISyntaxException {
        Todo savedClient = todoRepository.save(todo);
        return ResponseEntity.created(new URI("/todos/" + savedClient.getId())).body(savedClient);
    }

    //Editer une todo déjà existante via son id, méthode PUT qui édit dans la BDD
    @PutMapping("/{id}")
    public ResponseEntity<Todo> updateTodo(@PathVariable Long id, @RequestBody Todo todo) {
        Todo currentClient = todoRepository.findById(id).orElseThrow(RuntimeException::new);
        currentClient.setName(todo.getName());
        currentClient.setDescription(todo.getDescription());
        currentClient.setUrgent(todo.getUrgent());
        currentClient = todoRepository.save(todo);

        return ResponseEntity.ok(currentClient);
    }

    //Supprimer une todo, méthode DELETE qui supprime de la BDD
    @DeleteMapping("/{id}")
    public ResponseEntity<Todo> deleteTodo(@PathVariable Long id) {
        todoRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
