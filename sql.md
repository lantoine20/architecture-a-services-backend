java -cp ./lib/hsqldb.jar org.hsqldb.server.Server --database.0 file:firstBDD --dbname.0 firstBDD

CREATE TABLE todo ( id INT PRIMARY KEY NOT NULL, name VARCHAR(500), description VARCHAR(500))

INSERT INTO "PUBLIC"."TODO"
( "ID", "NAME", "DESCRIPTION" )
VALUES ( 1, 'Ménage', 'Passer aspirateur')
